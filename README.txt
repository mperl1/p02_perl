@author Matt Perl

The goal of this project is to make the traditional version of 2048. Additionally, it will include a "hard" difficulty that is equivalent to 4096 (start at 2048 and work down to 1) and an undo feature that allows the player to go back one move.  If time allows, I will implement a version where the tiles only move one square at a time.

Note: Please forgive the abysmal logic while learning the syntax on a time
crunch :p

Known Bugs
----------
1 - if a large number follows two smaller numbers that merge into that number,
  then all 3 will merge (e.g., 4 2 2 right merges into 8)

  potential fixes:
  1) keep track of which boxes merged (could use grid of (int, bool) pairs)
