//
//  ViewController.m
//  2048_Clone
//
//  Created by Matt Perl on 2/6/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//
//  Currently only formatted for iPhone7
//  Collaborated with Matt Spagnoli on shifting logic

#import "ViewController.h"
#include <stdlib.h>

@interface ViewController ()

@end

int currentScore = 0;
int lastScoreAdded = 0;
int undoCount = 1;
int numTilesCombined = 0;
int difficulty = 0; //0 = normal, 1 = hard
bool noMoreMoves = false;
NSMutableArray *row0;
NSMutableArray *row1;
NSMutableArray *row2;
NSMutableArray *row3;
int binGrid[4][4]; //represents grid, 0 = free, 1 = populated
int lastMoveGrid[4][4];

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpGame];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setUpGame{
    
    row0 = [[NSMutableArray alloc] init];
    row1 = [[NSMutableArray alloc] init];
    row2 = [[NSMutableArray alloc] init];
    row3 = [[NSMutableArray alloc] init];
    
    [row0 addObject: _gridImg00];
    [row0 addObject: _gridImg01];
    [row0 addObject: _gridImg02];
    [row0 addObject: _gridImg03];
    
    [row1 addObject: _gridImg10];
    [row1 addObject: _gridImg11];
    [row1 addObject: _gridImg12];
    [row1 addObject: _gridImg13];
    
    [row2 addObject: _gridImg20];
    [row2 addObject: _gridImg21];
    [row2 addObject: _gridImg22];
    [row2 addObject: _gridImg23];
    
    [row3 addObject: _gridImg30];
    [row3 addObject: _gridImg31];
    [row3 addObject: _gridImg32];
    [row3 addObject: _gridImg33];
    
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            binGrid[i][j] = 0;
            lastMoveGrid[i][j] = 0;
        }
    }
    [self generateRandomTile];
    [self generateRandomTile];
    [self updateLastMoveGrid];
    
}

- (void) generateRandomTile{
    int x1 = [self generateGridInd];
    int y1 = [self generateGridInd];
    
    bool slotOpen = false;
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            if(binGrid[i][j] == 0){
                slotOpen = true;
                break;
            }
        }
    }
    
    if(slotOpen == false){
        [self resetGame:NULL];
    }
    
    while(binGrid[x1][y1] > 0){
        x1 = [self generateGridInd];
        y1 = [self generateGridInd];
    }
    
    //printf("(%d, %d), (%d, %d)", x1, y1, x2, y2);
    
    UIImageView *temp;
    UIImage *startTile;
    
    if(difficulty == 0){
        startTile = [UIImage imageNamed:@"2.png"];
        binGrid[x1][y1] = 2;
    }
    else{
        startTile = [UIImage imageNamed:@"2048.png"];
        binGrid[x1][y1] = 2048;
    }
    
    if(x1 == 0){
        
        for(int i = 0; i < 4; i++){
            if(x1 == 0 && i == y1){
                temp = row0[i];
                [temp setImage: startTile];
            }
        }
    }
    if(x1 == 1){
        
        for(int i = 0; i < 4; i++){
            if(x1 == 1 && i == y1){
                temp = row1[i];
                [temp setImage: startTile];
            }
        }
    }
    if(x1 == 2){
        
        for(int i = 0; i < 4; i++){
            if(x1 == 2 && i == y1){
                temp = row2[i];
                [temp setImage: startTile];
            }
        }
    }
    if(x1 == 3){
        
        for(int i = 0; i < 4; i++){
            if(x1 == 3 && i == y1){
                temp = row3[i];
                [temp setImage: startTile];
            }
            
        }
    }
}

- (int) generateGridInd{
    int retVal = (arc4random() % 4);
    return retVal;
}

- (void)updateCurrentScore: (int)score{
    if(score == -1){
        [self updateHighScore: _currentScoreField.text];
        _currentScoreField.text = [NSString stringWithFormat:@"%d", 0];
        lastScoreAdded = 0;
    }
    else if(score == -2){ //last action was undone, subtract last score added
        int temp = [_currentScoreField.text intValue];
        temp -= lastScoreAdded;
        _currentScoreField.text = [NSString stringWithFormat:@"%d", temp];
    }
    else{
        int temp = [_currentScoreField.text intValue];
        if(difficulty == 1){
            switch(score)
            {
                case 4194304:
                    score = 4096;
                    break;
                case 2097152:
                    score = 2048;
                    break;
                case 1048576:
                    score = 1024;
                    break;
                case 524288:
                    score = 512;
                    break;
                case 262144:
                    score = 256;
                    break;
                case 131072:
                    score = 128;
                    break;
                case 65536:
                    score = 64;
                    break;
                case 32768:
                    score = 32;
                    break;
                case 16384:
                    score = 16;
                    break;
                case 8192:
                    score = 8;
                    break;
                case 4096:
                    score = 4;
                    break;
            }
        }
        
        temp += score;
        _currentScoreField.text = [NSString stringWithFormat:@"%d", temp];
        lastScoreAdded = score;
    }
}

- (void)updateHighScore: (NSString*)newHighScore{
    _highScoreField.text = newHighScore;
}

- (IBAction)setDifficultyNormal:(id)sender{
    if(difficulty == 1){
        difficulty = 0;
        [self resetGame:sender];
    }
}

- (IBAction)setDifficultyHard:(id)sender{
    if(difficulty == 0){
        difficulty = 1;
        [self resetGame:sender];
    }
}

- (IBAction)quitGame:(id)sender{
    exit(0);
}

- (IBAction)resetGame:(id)sender{
    UIImageView *temp0, *temp1, *temp2, *temp3;
    UIImage *resetTile = [UIImage imageNamed:@"tile.png"];
    for(int i = 0; i < 4; i++){
        temp0 = row0[i];
        [temp0 setImage: resetTile];
        temp1 = row1[i];
        [temp1 setImage: resetTile];
        temp2 = row2[i];
        [temp2 setImage: resetTile];
        temp3 = row3[i];
        [temp3 setImage: resetTile];
    }
    [row0 removeAllObjects];
    [row1 removeAllObjects];
    [row2 removeAllObjects];
    [row3 removeAllObjects];
    
    [self updateCurrentScore: -1];//tell currentScore to reset to 0
    [self setUpGame];
}

- (IBAction)undoMove:(id)sender{
    
    if(undoCount == 0){
        [self updatePlayerGrid:lastMoveGrid];
        undoCount = 1;
        
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                binGrid[i][j] = lastMoveGrid[i][j];
            }
        }
    }
    [self updateCurrentScore:-2];
}

- (void) updateLastMoveGrid{
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            lastMoveGrid[i][j] = binGrid[i][j];
        }
    }
}

- (void) updatePlayerGrid:(int[4][4]) inGrid{
    UIImageView *temp;
    UIImage *resetTile = [UIImage imageNamed:@"tile.png"];
    UIImage *tile1 = [UIImage imageNamed:@"1.png"];
    UIImage *tile2 = [UIImage imageNamed:@"2.png"];
    UIImage *tile4 = [UIImage imageNamed:@"4.png"];
    UIImage *tile8 = [UIImage imageNamed:@"8.png"];
    UIImage *tile16 = [UIImage imageNamed:@"16.png"];
    UIImage *tile32 = [UIImage imageNamed:@"32.png"];
    UIImage *tile64 = [UIImage imageNamed:@"64.png"];
    UIImage *tile128 = [UIImage imageNamed:@"128.png"];
    UIImage *tile256 = [UIImage imageNamed:@"256.png"];
    UIImage *tile512 = [UIImage imageNamed:@"512.png"];
    UIImage *tile1024 = [UIImage imageNamed:@"1024.png"];
    UIImage *tile2048 = [UIImage imageNamed:@"2048.png"];
    
    if(difficulty == 1){
        for(int i = 0; i < 4; i++){
            //row0 update
            temp = row0[i];
            switch(inGrid[0][i])
            {
                case 0:
                    [temp setImage: resetTile];
                    break;
                case 2048:
                    [temp setImage: tile2048];
                    break;
                case 4096:
                    [temp setImage: tile1024];
                    break;
                case 8192:
                    [temp setImage: tile512];
                    break;
                case 16384:
                    [temp setImage: tile256];
                    break;
                case 32768:
                    [temp setImage: tile128];
                    break;
                case 65536:
                    [temp setImage: tile64];
                    break;
                case 131072:
                    [temp setImage: tile32];
                    break;
                case 262144:
                    [temp setImage: tile16];
                    break;
                case 524288:
                    [temp setImage: tile8];
                    break;
                case 1048576:
                    [temp setImage: tile4];
                    break;
                case 2097152:
                    [temp setImage: tile2];
                    break;
                case 4194304:
                    [temp setImage: tile1];
                    break;
            }
            
            //row1 update
            temp = row1[i];
            switch(inGrid[1][i])
            {
                case 0:
                    [temp setImage: resetTile];
                    break;
                case 2048:
                    [temp setImage: tile2048];
                    break;
                case 4096:
                    [temp setImage: tile1024];
                    break;
                case 8192:
                    [temp setImage: tile512];
                    break;
                case 16384:
                    [temp setImage: tile256];
                    break;
                case 32768:
                    [temp setImage: tile128];
                    break;
                case 65536:
                    [temp setImage: tile64];
                    break;
                case 131072:
                    [temp setImage: tile32];
                    break;
                case 262144:
                    [temp setImage: tile16];
                    break;
                case 524288:
                    [temp setImage: tile8];
                    break;
                case 1048576:
                    [temp setImage: tile4];
                    break;
                case 2097152:
                    [temp setImage: tile2];
                    break;
                case 4194304:
                    [temp setImage: tile1];
                    break;
            }
            
            //row2 update
            temp = row2[i];
            switch(inGrid[2][i])
            {
                case 0:
                    [temp setImage: resetTile];
                    break;
                case 2048:
                    [temp setImage: tile2048];
                    break;
                case 4096:
                    [temp setImage: tile1024];
                    break;
                case 8192:
                    [temp setImage: tile512];
                    break;
                case 16384:
                    [temp setImage: tile256];
                    break;
                case 32768:
                    [temp setImage: tile128];
                    break;
                case 65536:
                    [temp setImage: tile64];
                    break;
                case 131072:
                    [temp setImage: tile32];
                    break;
                case 262144:
                    [temp setImage: tile16];
                    break;
                case 524288:
                    [temp setImage: tile8];
                    break;
                case 1048576:
                    [temp setImage: tile4];
                    break;
                case 2097152:
                    [temp setImage: tile2];
                    break;
                case 4194304:
                    [temp setImage: tile1];
                    break;
            }
            
            //row3 update
            temp = row3[i];
            switch(inGrid[3][i])
            {
                case 0:
                    [temp setImage: resetTile];
                    break;
                case 2048:
                    [temp setImage: tile2048];
                    break;
                case 4096:
                    [temp setImage: tile1024];
                    break;
                case 8192:
                    [temp setImage: tile512];
                    break;
                case 16384:
                    [temp setImage: tile256];
                    break;
                case 32768:
                    [temp setImage: tile128];
                    break;
                case 65536:
                    [temp setImage: tile64];
                    break;
                case 131072:
                    [temp setImage: tile32];
                    break;
                case 262144:
                    [temp setImage: tile16];
                    break;
                case 524288:
                    [temp setImage: tile8];
                    break;
                case 1048576:
                    [temp setImage: tile4];
                    break;
                case 2097152:
                    [temp setImage: tile2];
                    break;
                case 4194304:
                    [temp setImage: tile1];
                    break;
            }
        }
    }
    else{
        for(int i = 0; i < 4; i++){
            //row0 update
            temp= row0[i];
            switch(inGrid[0][i])
            {
                case 0:
                    [temp setImage: resetTile];
                    break;
                case 1:
                    [temp setImage: tile1];
                    break;
                case 2:
                    [temp setImage: tile2];
                    break;
                case 4:
                    [temp setImage: tile4];
                    break;
                case 8:
                    [temp setImage: tile8];
                    break;
                case 16:
                    [temp setImage: tile16];
                    break;
                case 32:
                    [temp setImage: tile32];
                    break;
                case 64:
                    [temp setImage: tile64];
                    break;
                case 128:
                    [temp setImage: tile128];
                    break;
                case 256:
                    [temp setImage: tile256];
                    break;
                case 512:
                    [temp setImage: tile512];
                    break;
                case 1024:
                    [temp setImage: tile1024];
                    break;
                case 2048:
                    [temp setImage: tile2048];
                    break;
            }
            
            //row1 update
            temp = row1[i];
            switch(inGrid[1][i])
            {
                case 0:
                    [temp setImage: resetTile];
                    break;
                case 1:
                    [temp setImage: tile1];
                    break;
                case 2:
                    [temp setImage: tile2];
                    break;
                case 4:
                    [temp setImage: tile4];
                    break;
                case 8:
                    [temp setImage: tile8];
                    break;
                case 16:
                    [temp setImage: tile16];
                    break;
                case 32:
                    [temp setImage: tile32];
                    break;
                case 64:
                    [temp setImage: tile64];
                    break;
                case 128:
                    [temp setImage: tile128];
                    break;
                case 256:
                    [temp setImage: tile256];
                    break;
                case 512:
                    [temp setImage: tile512];
                    break;
                case 1024:
                    [temp setImage: tile1024];
                    break;
                case 2048:
                    [temp setImage: tile2048];
                    break;
            }
            
            //row2 update
            temp = row2[i];
            switch(inGrid[2][i])
            {
                case 0:
                    [temp setImage: resetTile];
                    break;
                case 1:
                    [temp setImage: tile1];
                    break;
                case 2:
                    [temp setImage: tile2];
                    break;
                case 4:
                    [temp setImage: tile4];
                    break;
                case 8:
                    [temp setImage: tile8];
                    break;
                case 16:
                    [temp setImage: tile16];
                    break;
                case 32:
                    [temp setImage: tile32];
                    break;
                case 64:
                    [temp setImage: tile64];
                    break;
                case 128:
                    [temp setImage: tile128];
                    break;
                case 256:
                    [temp setImage: tile256];
                    break;
                case 512:
                    [temp setImage: tile512];
                    break;
                case 1024:
                    [temp setImage: tile1024];
                    break;
                case 2048:
                    [temp setImage: tile2048];
                    break;
            }
            
            //row3 update
            temp = row3[i];
            switch(inGrid[3][i])
            {
                case 0:
                    [temp setImage: resetTile];
                    break;
                case 1:
                    [temp setImage: tile1];
                    break;
                case 2:
                    [temp setImage: tile2];
                    break;
                case 4:
                    [temp setImage: tile4];
                    break;
                case 8:
                    [temp setImage: tile8];
                    break;
                case 16:
                    [temp setImage: tile16];
                    break;
                case 32:
                    [temp setImage: tile32];
                    break;
                case 64:
                    [temp setImage: tile64];
                    break;
                case 128:
                    [temp setImage: tile128];
                    break;
                case 256:
                    [temp setImage: tile256];
                    break;
                case 512:
                    [temp setImage: tile512];
                    break;
                case 1024:
                    [temp setImage: tile1024];
                    break;
                case 2048:
                    [temp setImage: tile2048];
                    break;
            }
        }
    }
}

- (IBAction)shiftLeft:(id)sender{
    [self updateLastMoveGrid];
    
    bool hasMerged;
    bool canShift;
    int currentLev;
    
    for(int i = 0; i < 4; i++){
        for(int j = 1; j <= 3; j++){
            hasMerged = false;
            canShift = true;
            currentLev = j;
            while(binGrid[i][currentLev] != 0 && canShift == true){
                //check if bottom slot is empty
                if(binGrid[i][currentLev-1] == 0){
                    binGrid[i][currentLev-1] = binGrid[i][currentLev];
                    binGrid[i][currentLev] = 0;
                }
                //check if merges are possible
                else if(hasMerged == false && (binGrid[i][currentLev-1] == binGrid[i][currentLev])){
                    binGrid[i][currentLev-1] += binGrid[i][currentLev];
                    binGrid[i][currentLev] = 0;
                    hasMerged = true;
                    [self updateCurrentScore:binGrid[i][currentLev-1]];
                }
                
                currentLev--;
                if(currentLev == 0){
                    canShift = false;
                }
            }
        }
    }
    
    [self updatePlayerGrid:binGrid];
    [self generateRandomTile];
    undoCount = 0;
}

- (IBAction)shiftUp:(id)sender{
    [self updateLastMoveGrid];
    
    bool hasMerged;
    bool canShift;
    int currentLev;
    
    for(int i = 1; i <= 3; i++){
        for(int j = 0; j < 4; j++){
            hasMerged = false;
            canShift = true;
            currentLev = i;
            while(binGrid[currentLev][j] != 0 && canShift == true){
                //check if bottom slot is empty
                if(binGrid[currentLev-1][j] == 0){
                    binGrid[currentLev-1][j] = binGrid[currentLev][j];
                    binGrid[currentLev][j] = 0;
                }
                //check if merges are possible
                else if(hasMerged == false && (binGrid[currentLev-1][j] == binGrid[currentLev][j])){
                    binGrid[currentLev-1][j] += binGrid[currentLev][j];
                    binGrid[currentLev][j] = 0;
                    hasMerged = true;
                    [self updateCurrentScore:binGrid[currentLev-1][j]];
                }
                
                currentLev--;
                if(currentLev == 0){
                    canShift = false;
                }
            }
        }
    }
    
    [self updatePlayerGrid:binGrid];
    [self generateRandomTile];
    undoCount = 0;
}

- (IBAction)shiftRight:(id)sender{
    [self updateLastMoveGrid];
    
    bool hasMerged;
    bool canShift;
    int currentLev;
    
    for(int i = 0; i < 4; i++){
        for(int j = 2; j >= 0; j--){
            hasMerged = false;
            canShift = true;
            currentLev = j;
            while(binGrid[i][currentLev] != 0 && canShift == true){
                //check if bottom slot is empty
                if(binGrid[i][currentLev+1] == 0){
                    binGrid[i][currentLev+1] = binGrid[i][currentLev];
                    binGrid[i][currentLev] = 0;
                }
                //check if merges are possible
                else if(hasMerged == false && (binGrid[i][currentLev+1] == binGrid[i][currentLev])){
                    binGrid[i][currentLev+1] += binGrid[i][currentLev];
                    binGrid[i][currentLev] = 0;
                    hasMerged = true;
                    [self updateCurrentScore:binGrid[i][currentLev+1]];
                }
                
                currentLev++;
                if(currentLev == 3){
                    canShift = false;
                }
            }
        }
    }
    
    [self updatePlayerGrid:binGrid];
    [self generateRandomTile];
    undoCount = 0;
}

- (IBAction)shiftDown:(id)sender{
    [self updateLastMoveGrid];
    bool hasMerged;
    bool canShift;
    int currentLev;
    
    for(int i = 2; i >= 0; i--){
        for(int j = 0; j < 4; j++){
            hasMerged = false;
            canShift = true;
            currentLev = i;
            while(binGrid[currentLev][j] != 0 && canShift == true){
                //check if bottom slot is empty
                if(binGrid[currentLev+1][j] == 0){
                    binGrid[currentLev+1][j] = binGrid[currentLev][j];
                    binGrid[currentLev][j] = 0;
                }
                //check if merges are possible
                else if(hasMerged == false && (binGrid[currentLev+1][j] == binGrid[currentLev][j])){
                    binGrid[currentLev+1][j] += binGrid[currentLev][j];
                    binGrid[currentLev][j] = 0;
                    hasMerged = true;
                    [self updateCurrentScore:binGrid[currentLev+1][j]];
                }
                
                currentLev++;
                if(currentLev == 3){
                    canShift = false;
                }
            }
        }
    }
    
    [self updatePlayerGrid:binGrid];
    [self generateRandomTile];
    undoCount = 0;
}

@end
