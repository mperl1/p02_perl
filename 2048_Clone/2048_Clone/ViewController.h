//
//  ViewController.h
//  2048_Clone
//
//  Created by Matt Perl on 2/6/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//
//  Currently only formatted for iPhone7
//  Collaborated with Matt Spagnoli on shifting logic

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *currentScoreField;

@property (strong, nonatomic) IBOutlet UITextField *highScoreField;

@property (strong, nonatomic) IBOutlet UIButton *normalDifficultyButton;

@property (strong, nonatomic) IBOutlet UIButton *hardDifficultyButton;

@property (strong, nonatomic) IBOutlet UIButton *undoButton;

@property (strong, nonatomic) IBOutlet UIButton *quitButton;

@property (strong, nonatomic) IBOutlet UIButton *resetButton;

@property (strong, nonatomic) IBOutlet UIButton *shiftUpButton;

@property (strong, nonatomic) IBOutlet UIButton *shiftRightButton;

@property (strong, nonatomic) IBOutlet UIButton *shiftDownButton;

@property (strong, nonatomic) IBOutlet UIButton *shiftLeftButton;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg00;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg01;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg02;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg03;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg10;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg11;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg12;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg13;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg20;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg21;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg22;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg23;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg30;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg31;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg32;

@property (strong, nonatomic) IBOutlet UIImageView *gridImg33;


@end

